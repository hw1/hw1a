/* Homework 1, 600.120 Spring 2015                                                    
                                                                                      
   Reference implementation by Ben Mitchell, ben@cs.jhu.edu                           
   Programmer: Sang Yeon Kim (skim340)
               Matthew Chen (mchen93)
   I used the solution for hw0                                                        
*/

#include <stdio.h>
#include <string.h>
#include <math.h>
#define LENGTH 16

void printMenu(void){
  printf("n - display the total number of courses");
  printf("\n");
  printf("d - list all courses from a particular department (prompt for number)");
  printf("\n");
  printf("l - list all courses with a particular letter grade  (prompt for grade)");
  printf("\n");
  printf("c - list all courses with at least a specified number of credits (prompt for credits)");
  printf("\n");
  printf("g - compute the GPA of all the courses with letter grades (skipping those with 'I', 'S' or 'U' grades)\
");
  printf("\n");
  printf("q - quit the program");
  printf("\n");
  printf("Enter letter choice -> ");
}


int main(void){
  //users choice
  char choice;
  //users input for department
  unsigned int deptChoice;
  //file input
  FILE* input = fopen("courseInts.txt", "r");
  unsigned int nextDecimal;
  //arrays that store the values after system reads it
  unsigned int decimalArray[2500];
  unsigned int DeptArray[2500];
  unsigned int gradeArray[2500];
  unsigned int markArray[2500];
  unsigned int creditArray[2500];
  unsigned int halfCreditArray[2500];
  unsigned int gpaArray[2500];
  int position = 0;
  int numberEntry = 0;
  int go = 1;
  while(fscanf(input,"%u",&nextDecimal) != EOF){    
    decimalArray[position] = nextDecimal;
    position ++;
    numberEntry ++; //counts how many entires
  }
  
  while(go){
    printMenu();
    scanf(" %c",&choice);
    if(choice == 'q'){
      go = 0;//loops the menu until q is pressed
    }
    else if(choice == 'n'){
      printf("%d\n",numberEntry);
    }
    else if(choice == 'g'){
      int i = 0;
      float gpaTotal;
      unsigned int gpaGradeArray[2500];
      unsigned int gpaMarkArray[2500];
      unsigned int gpacreditArray[2500];
      unsigned int gpahalfCreditArray[2500];
      float gpaCourse;
      float gpaCounter;
      float gpaCredit;
      float gpaTotalCredit;
      for(i = 0; i < position; i++){
	//00000000000000000000000111000000
	gpaArray[i] = decimalArray[i] << 23;
        gpaArray[i] = gpaArray[i] >> 29;
	if((gpaArray[i] != 5)&&(gpaArray[i] != 6)&&(gpaArray[i] != 7)){
	  gpaCounter ++;
	  gpaGradeArray[i] = decimalArray[i] << 23;
	  gpaGradeArray[i] = gpaGradeArray[i] >> 29;
	  gpaMarkArray[i] = decimalArray[i] << 26;
	  gpaMarkArray[i] = gpaMarkArray[i] >> 30;
	  gpacreditArray[i] = decimalArray[i] << 28;
	  gpacreditArray[i] = gpacreditArray[i] >> 29;
	  gpahalfCreditArray[i] = decimalArray[i] << 31;
	  gpahalfCreditArray[i] = gpahalfCreditArray[i] >> 31;
	  if(gpaGradeArray[i] == 0 && gpaMarkArray[i] == 0){
	    gpaCourse = 4.0;
	  }
	  else if(gpaGradeArray[i] == 0 && gpaMarkArray[i] == 1){
	    gpaCourse = 3.7;
	  }
	  else if(gpaGradeArray[i] == 0 && gpaMarkArray[i] == 2){
            gpaCourse = 4.0;
          }
	  else if(gpaGradeArray[i] == 1 && gpaMarkArray[i] == 0){
            gpaCourse =3.3;
          }
	  else if(gpaGradeArray[i] == 1 && gpaMarkArray[i] == 1){
            gpaCourse =2.7;
          }
	  else if(gpaGradeArray[i] == 1 && gpaMarkArray[i] == 2){
            gpaCourse =3.0;
          }
	  else if(gpaGradeArray[i] == 2 && gpaMarkArray[i] == 0){
            gpaCourse =2.3;
          }
	  else if(gpaGradeArray[i] == 2 && gpaMarkArray[i] == 1){
            gpaCourse =1.7;
          }
	  else if(gpaGradeArray[i] == 2 && gpaMarkArray[i] == 2){
            gpaCourse =2.0;
          }
	  else if(gpaGradeArray[i] == 3 && gpaMarkArray[i] == 0){
            gpaCourse =1.3;
          }
	  else if(gpaGradeArray[i] == 3 && gpaMarkArray[i] == 2){
            gpaCourse =1.0;
          }
	  else if(gpaGradeArray[i] == 4){
            gpaCourse =0.0;
          }
	  if(gpahalfCreditArray[i] == 0){
	    gpaCredit = gpacreditArray[i];
	  }
	  else if(gpahalfCreditArray[i] == 1){
	    gpaCredit = gpacreditArray[i] + 0.5;
	  }
	  gpaTotal = gpaTotal + (gpaCourse * gpaCredit);
	  gpaTotalCredit += gpaCredit;
	}
      }
      float gpaFinal;
      gpaFinal = (gpaTotal)/gpaTotalCredit;
      printf("Your GPA is %f\n",gpaFinal);
    }
    else if(choice == 'c'){
      printf("Please input your credits:  ");
      float nextCredit;
      scanf(" %f", &nextCredit);
      int i;
      for(i = 0; i < position; i++){
        //00000000000000000000000000001110                                                                      
        creditArray[i] = decimalArray[i] << 28;
        creditArray[i] = creditArray[i] >> 29;
	halfCreditArray[i] = decimalArray[i] << 31;
        halfCreditArray[i] = halfCreditArray[i] >> 31;
	float convertedCredit;
	if(halfCreditArray[i] == 0){
	  convertedCredit = 0;
	}
	else if(halfCreditArray[i] == 1){
	  convertedCredit = 0.5;
	}
	if((float)creditArray[i] + convertedCredit  <= nextCredit){
	  if((decimalArray[i] >> 29) == 0){
            printf("AS.");
            //00011111111110000000000000000000                                                                          
            printf("%03d.",(decimalArray[i] << 3) >> 22);
            //11111111110000000000000000000000                                                                           
            printf("%03d",(decimalArray[i] << 13) >> 22);
            //00000000000000000000000111000000                                                                           
            if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
            //00000000000000000000000000110000                                                                           

            if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
	    if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
            else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
            //00000000000000000000000000000001                                                                           
            if((decimalArray[i] << 31) >> 31 == 0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }
	  }
	  if((decimalArray[i] >> 29) == 1){
            printf("BU.");
            //00011111111110000000000000000000                                                                            
            printf("%03d.",(decimalArray[i] << 3) >> 22);
            //11111111110000000000000000000000                                                                            
            printf("%03d",(decimalArray[i] << 13) >> 22);
            //00000000000000000000000111000000                                                                            
            if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
            //00000000000000000000000000110000                                                                            
            if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
	    if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
            else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
            //00000000000000000000000000000001                                                                            
            if((decimalArray[i] << 31) >> 31 == 0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }
	  }
	  if((decimalArray[i] >> 29) == 2){
            printf("ED.");
            //00011111111110000000000000000000                                                                            
            printf("%03d.",(decimalArray[i] << 3) >> 22);
            //11111111110000000000000000000000                                                                            
            printf("%03d",(decimalArray[i] << 13) >> 22);
            //00000000000000000000000111000000                                                                            
            if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
            //00000000000000000000000000110000                                                                            
            if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
	    if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
            else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
            //00000000000000000000000000000001                                                                            
            if((decimalArray[i] << 31) >> 31 == 0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }
	  }
	  if((decimalArray[i] >> 29) == 3){
            printf("EN.");
            //00011111111110000000000000000000                                                                            
            printf("%03d.",(decimalArray[i] << 3) >> 22);
            //11111111110000000000000000000000                                                                            
            printf("%03d",(decimalArray[i] << 13) >> 22);
            //00000000000000000000000111000000                                                                            
            if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
            //00000000000000000000000000110000                                                                            
            if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
	    if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
            else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
            //00000000000000000000000000000001                                                                            
            if((decimalArray[i] << 31) >> 31 == 0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }
	  }
	  if((decimalArray[i] >> 29) == 4){
            printf("ME.");
            //00011111111110000000000000000000                                                                            
            printf("%03d.",(decimalArray[i] << 3) >> 22);
            //11111111110000000000000000000000                                                                            
            printf("%03d",(decimalArray[i] << 13) >> 22);
            //00000000000000000000000111000000                                                                            
            if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
            //00000000000000000000000000110000                                                                            
            if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
	    if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
            else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
            //00000000000000000000000000000001                                                                            
            if((decimalArray[i] << 31) >> 31 == 0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }
	  }
	  if((decimalArray[i] >> 29) == 5){
            printf("PH.");
            //00011111111110000000000000000000                                                                            
            printf("%03d.",(decimalArray[i] << 3) >> 22);
            //11111111110000000000000000000000                                                                            
            printf("%03d",(decimalArray[i] << 13) >> 22);
            //00000000000000000000000111000000                                                                            
            if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
            //00000000000000000000000000110000                                                                            
            if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
	    if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
            else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
            //00000000000000000000000000000001                                                                            
            if((decimalArray[i] << 31) >> 31 == 0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }
	  }
	  if((decimalArray[i] >> 29) == 6){
            printf("PY.");
            //00011111111110000000000000000000                                                                            
            printf("%03d.",(decimalArray[i] << 3) >> 22);
            //11111111110000000000000000000000                                                                            
            printf("%03d",(decimalArray[i] << 13) >> 22);
            //00000000000000000000000111000000                                                                            
            if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
            //00000000000000000000000000110000                                                                            
            if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
	    if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
            else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
            //00000000000000000000000000000001                                                                            
            if((decimalArray[i] << 31) >> 31 == 0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }
	  }

	  if((decimalArray[i] >> 29) == 7){
            printf("SA.");
            //00011111111110000000000000000000                                                                            
            printf("%03d.",(decimalArray[i] << 3) >> 22);
            //11111111110000000000000000000000                                                                            
            printf("%03d",(decimalArray[i] << 13) >> 22);
            //00000000000000000000000111000000                                                                            
            if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
            //00000000000000000000000000110000                                                                            
            if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
	    if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
            else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
            //00000000000000000000000000000001                                                                            
            if((decimalArray[i] << 31) >> 31 == 0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }
	  }
	}
      }
    }
    else if(choice == 'l'){
      printf("Please input your grade:  ");
      char gradeChoice;
      char markChoice;
      scanf(" %c%c", &gradeChoice,&markChoice);
      unsigned int convertedMark;
      unsigned int convertedGrade;
      if(markChoice == '+'){
	convertedMark = 0;
      }
      else if(markChoice == '-'){
	convertedMark = 1;
      }
      else if(markChoice == '/'){
	convertedMark = 2;
      }
      if(gradeChoice == 'A'){
	convertedGrade = 0;
      }
      else if(gradeChoice == 'B'){
	convertedGrade = 1;
      }
      else if(gradeChoice == 'C'){
	convertedGrade = 2;
      }
      else if(gradeChoice == 'D'){
	convertedGrade = 3;
      }
      else if(gradeChoice == 'F'){
	convertedGrade = 4;
      }
      else if(gradeChoice == 'I'){
	convertedGrade = 5;
      }
      else if(gradeChoice == 'S'){
	convertedGrade = 6;
      }
      else if(gradeChoice == 'U'){
	convertedGrade = 7;
      }
      int i;
      for(i = 0; i < position; i++){
	//00000000000000000000000111000000 
        gradeArray[i] = decimalArray[i] << 23;         
        gradeArray[i] = gradeArray[i] >> 29;
	//00000000000000000000000000110000
	markArray[i] = decimalArray[i] << 26;
	//11000000000000000000000000000000
	markArray[i] = markArray[i] >> 30; 
	if((gradeArray[i] == convertedGrade) && (markArray[i] == convertedMark)){
	  if((decimalArray[i] >> 29) == 0){
            printf("AS.");
	    //00011111111110000000000000000000
	    printf("%03d.",(decimalArray[i] << 3) >> 22);
	    //11111111110000000000000000000000
            printf("%03d",(decimalArray[i] << 13) >> 22);
            //00000000000000000000000111000000                                                               
            if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
            //00000000000000000000000000110000                                                               
            if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
	      printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
            //00000000000000000000000000001110                                                               
            if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
            else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
            //00000000000000000000000000000001                                                             
            if((decimalArray[i] << 31) >> 31 == 0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }
	  }
	  if((decimalArray[i] >> 29) == 1){
	    printf("BU.");
	    //00011111111110000000000000000000
	    printf("%03d.",(decimalArray[i] << 3) >> 22);
	    //11111111110000000000000000000000
	    printf("%03d",(decimalArray[i] << 13) >> 22);
	    //00000000000000000000000111000000                                                            
    
	    if((decimalArray[i] << 23) >> 29 == 0){
	      printf("A");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==1){
	      printf("B");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==2){
	      printf("C");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==3){
	      printf("D");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==4){
	      printf("F");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==5){
	      printf("I");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==6){
	      printf("S");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==7){
	      printf("U");
	    }
	    //00000000000000000000000000110000
	    if((decimalArray[i] << 26) >> 30 == 0){
	      printf("+");
	    }
	    else if((decimalArray[i] << 26) >> 30 == 1){
	      printf("-");
	    }
	    else if((decimalArray[i] << 26) >> 30 == 2){
	      printf("/");
	    }
	    //00000000000000000000000000001110                                                             
    
	    if((decimalArray[i] << 28) >> 29 == 0){
	      printf("0.");
	    }
	    else if((decimalArray[i] << 28) >> 29 == 1){
	      printf("1.");
	    }
	    else if((decimalArray[i] << 28) >> 29 == 2){
	      printf("2.");
	    }
	    else if((decimalArray[i] << 28) >> 29 == 3){
	      printf("3.");
	    }
	    else if((decimalArray[i] << 28) >> 29 == 4){
	      printf("4.");
	    }
	    else if((decimalArray[i] << 28) >> 29 == 5){
	      printf("5.");
	    }
	    //00000000000000000000000000000001                                                          
    
	    if((decimalArray[i] << 31) >> 31 == 0){
	      printf("0");
	      printf("\n");
	    }
	    else if((decimalArray[i] << 31) >> 31 ==1){
	      printf("5");
	      printf("\n");
	    }
	  }
	  if((decimalArray[i] >> 29) == 2){
	    printf("ED.");
	    //00011111111110000000000000000000
	    printf("%03d.",(decimalArray[i] << 3) >> 22);
	    //11111111110000000000000000000000
	    printf("%03d",(decimalArray[i] << 13) >> 22);
	    //00000000000000000000000111000000                                                              
    
	    if((decimalArray[i] << 23) >> 29 == 0){
	      printf("A");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==1){
	      printf("B");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==2){
	      printf("C");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==3){
	      printf("D");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==4){
	      printf("F");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==5){
	      printf("I");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==6){
	      printf("S");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==7){
	      printf("U");
	    }
	    //00000000000000000000000000110000
	    if((decimalArray[i] << 26) >> 30 == 0){
	      printf("+");
	    }
	    else if((decimalArray[i] << 26) >> 30 == 1){
	      printf("-");
	    }
	    else if((decimalArray[i] << 26) >> 30 == 2){
	      printf("/");
	    }
	    //00000000000000000000000000001110                                                             
	    if((decimalArray[i] << 28) >> 29 == 0){
	      printf("0.");
	    }
	    else if((decimalArray[i] << 28) >> 29 == 1){
	      printf("1.");
	    }
	    else if((decimalArray[i] << 28) >> 29 == 2){
	      printf("2.");
	    }
	    else if((decimalArray[i] << 28) >> 29 == 3){
	      printf("3.");
	    }
	    else if((decimalArray[i] << 28) >> 29 == 4){
	      printf("4.");
	    }
	    else if((decimalArray[i] << 28) >> 29 == 5){
	      printf("5.");
	    }
	    //00000000000000000000000000000001                                                              
	    if((decimalArray[i] << 31) >> 31 == 0){
	      printf("0");
	      printf("\n");
	    }
	    else if((decimalArray[i] << 31) >> 31 ==1){
	      printf("5");
	      printf("\n");
	    }
	  }
	  if((decimalArray[i] >> 29) == 3){
	    printf("EN.");
	    //00011111111110000000000000000000
	    printf("%03d.",(decimalArray[i] << 3) >> 22);
	    //11111111110000000000000000000000
	    printf("%03d",(decimalArray[i] << 13) >> 22);
	    //00000000000000000000000111000000                                                              
    
	    if((decimalArray[i] << 23) >> 29 == 0){
	      printf("A");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==1){
	      printf("B");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==2){
	      printf("C");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==3){
	      printf("D");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==4){
	      printf("F");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==5){
	      printf("I");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==6){
	      printf("S");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==7){
	      printf("U");
	    }
	    //00000000000000000000000000110000
	    if((decimalArray[i] << 26) >> 30 == 0){
	      printf("+");
	    }
	    else if((decimalArray[i] << 26) >> 30 == 1){
	      printf("-");
	    }
	    else if((decimalArray[i] << 26) >> 30 == 2){
	      printf("/");
	    }
	    //00000000000000000000000000001110                                                              
    
	    if((decimalArray[i] << 28) >> 29 == 0){
	      printf("0.");
	    }
	    else if((decimalArray[i] << 28) >> 29 == 1){
	      printf("1.");
	    }
	    else if((decimalArray[i] << 28) >> 29 == 2){
	      printf("2.");
	    }
	    else if((decimalArray[i] << 28) >> 29 == 3){
	      printf("3.");
	    }
	    else if((decimalArray[i] << 28) >> 29 == 4){
	      printf("4.");
	    }
	    else if((decimalArray[i] << 28) >> 29 == 5){
	      printf("5.");
	    }
	    //00000000000000000000000000000001                                                             
    
	    if((decimalArray[i] << 31) >> 31 == 0){
	      printf("0");
	      printf("\n");
	    }
	    else if((decimalArray[i] << 31) >> 31 ==1){
	      printf("5");
	      printf("\n");
	    }
	  }
	  if((decimalArray[i] >> 29) == 4){
	    printf("ME.");
	    //00011111111110000000000000000000                                                               
            printf("%03d.",(decimalArray[i] << 3) >> 22);
            //11111111110000000000000000000000                                                               
            printf("%03d",(decimalArray[i] << 13) >> 22);
            //00000000000000000000000111000000                                                              

            if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
            //00000000000000000000000000110000                                                               
            if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
	    if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
            else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
            //00000000000000000000000000000001                                                               

            if((decimalArray[i] << 31) >> 31 == 0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }
	  }
	  if((decimalArray[i] >> 29) == 5){
            printf("PH.");
	    //00011111111110000000000000000000                                                             
                                                                                                           
            printf("%03d.",(decimalArray[i] << 3) >> 22);
            //11111111110000000000000000000000                                                            
                                                                                                           
            printf("%03d",(decimalArray[i] << 13) >> 22);
            //00000000000000000000000111000000                                                              

            if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
	    if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
            if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
            else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
	    if((decimalArray[i] << 31) >> 31 == 0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }

	  }
	  if((decimalArray[i] >> 29) == 6){
            printf("PY.");
	    printf("%03d.",(decimalArray[i] << 3) >> 22);
            //11111111110000000000000000000000                                                               

            printf("%03d",(decimalArray[i] << 13) >> 22);
            //00000000000000000000000111000000                                                              

            if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
            if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
            if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
	    }
	    else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
            if((decimalArray[i] << 31) >> 31 == 0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }
	  }
	  if((decimalArray[i] >> 29) == 7){
            printf("SA.");
	    printf("%03d.",(decimalArray[i] << 3) >> 22);
            //11111111110000000000000000000000                                                             
                                                                                                             

            printf("%03d",(decimalArray[i] << 13) >> 22);
            //00000000000000000000000111000000                                                              

            if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
            if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
	    if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
            else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
            if((decimalArray[i] << 31) >> 31 == 0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }

	  }
	}
      }
    }
    else if(choice == 'd'){
      printf("Please input the department number:  ");
      scanf(" %u", &deptChoice);
      int i;
      for(i = 0; i < position; i++){
	//00011111111110000000000000000000
	DeptArray[i] = decimalArray[i] << 3;
	//11111111110000000000000000000000
	DeptArray[i] = DeptArray[i] >> 22;  
	if(DeptArray[i] == deptChoice){
	  //printf("Works \n");
	  if((decimalArray[i] >> 29) == 0){
	    printf("AS.%u.",deptChoice);
	    printf("%03d",(decimalArray[i] << 13) >> 22);
	    //00000000000000000000000111000000
	    if((decimalArray[i] << 23) >> 29 == 0){
	      printf("A");
	    }
	    else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
	    else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
	    else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
	    else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
	    else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
	    else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
	    else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
	    //00000000000000000000000000110000
	    if((decimalArray[i] << 26) >> 30 == 0){
	      printf("+");
	    }
	    else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
	    else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
	    //00000000000000000000000000001110
	    if((decimalArray[i] << 28) >> 29 == 0){
	      printf("0.");
	    }
	    else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
	    else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
	    else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
	    else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
	    else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
	    //00000000000000000000000000000001
	    if((decimalArray[i] << 31) >> 31 == 0){
	      printf("0");
	      printf("\n");
	    }
	    else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }
	  }
	  if((decimalArray[i] >> 29) == 1){
	    printf("BU.%u.",deptChoice);
	    printf("%03d",(decimalArray[i] << 13) >> 22);
	    if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
	    if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
	    //00000000000000000000000000001110                                                            
            if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
            else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
            //00000000000000000000000000000001                                                            
            if((decimalArray[i] << 31) >> 31 ==0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }
	  }
	  if((decimalArray[i] >> 29) == 2){
            printf("ED.%u.",deptChoice);
	    printf("%03d",(decimalArray[i] << 13) >> 22);
	    if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
	    if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
	    //00000000000000000000000000001110                                                            
            if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
            else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
            //00000000000000000000000000000001                                                            
            if((decimalArray[i] << 31) >> 31 ==0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }
	  }
	  if((decimalArray[i] >> 29) == 3){
            printf("EN.%u.",deptChoice);
	    printf("%03d",(decimalArray[i] << 13) >> 22);
	    if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
	    if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
	    //00000000000000000000000000001110                                                            
            if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
            else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
            //00000000000000000000000000000001                                                            
            if((decimalArray[i] << 31) >> 31 ==0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }
          }
	  if((decimalArray[i] >> 29) == 4){
            printf("ME.%u.",deptChoice);
	    printf("%03d",(decimalArray[i] << 13) >> 22);
	    if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
	    if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
	    //00000000000000000000000000001110                                                            
            if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
            else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
            //00000000000000000000000000000001                                                            
            if((decimalArray[i] << 31) >> 31 ==0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }
          }
	  if((decimalArray[i] >> 29) == 5){
            printf("PH.%u.",deptChoice);
	    printf("%03d",(decimalArray[i] << 13) >> 22);
	    if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
	    if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
	    //00000000000000000000000000001110                                                            
            if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
            else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
            //00000000000000000000000000000001                                                            
            if((decimalArray[i] << 31) >> 31 ==0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }
          }
	  if((decimalArray[i] >> 29) == 6){
            printf("PY.%u.",deptChoice);
	    printf("%03d",(decimalArray[i] << 13) >> 22);
	    if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
	    if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
	    //00000000000000000000000000001110                                                            
            if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
            else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
            //00000000000000000000000000000001                                                            
            if((decimalArray[i] << 31) >> 31 ==0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }
          }
	  if((decimalArray[i] >> 29) == 7){
            printf("SA.%u.",deptChoice);
	    printf("%03d",(decimalArray[i] << 13) >> 22);
	    if((decimalArray[i] << 23) >> 29 == 0){
              printf("A");
            }
            else if((decimalArray[i] << 23) >> 29 ==1){
              printf("B");
            }
            else if((decimalArray[i] << 23) >> 29 ==2){
              printf("C");
            }
            else if((decimalArray[i] << 23) >> 29 ==3){
              printf("D");
            }
            else if((decimalArray[i] << 23) >> 29 ==4){
              printf("F");
            }
            else if((decimalArray[i] << 23) >> 29 ==5){
              printf("I");
            }
            else if((decimalArray[i] << 23) >> 29 ==6){
              printf("S");
            }
            else if((decimalArray[i] << 23) >> 29 ==7){
              printf("U");
            }
	    if((decimalArray[i] << 26) >> 30 == 0){
              printf("+");
            }
            else if((decimalArray[i] << 26) >> 30 == 1){
              printf("-");
            }
            else if((decimalArray[i] << 26) >> 30 == 2){
              printf("/");
            }
	    //00000000000000000000000000001110                                                            
            if((decimalArray[i] << 28) >> 29 == 0){
              printf("0.");
            }
            else if((decimalArray[i] << 28) >> 29 == 1){
              printf("1.");
            }
            else if((decimalArray[i] << 28) >> 29 == 2){
              printf("2.");
            }
            else if((decimalArray[i] << 28) >> 29 == 3){
              printf("3.");
            }
            else if((decimalArray[i] << 28) >> 29 == 4){
              printf("4.");
            }
            else if((decimalArray[i] << 28) >> 29 == 5){
              printf("5.");
            }
            //00000000000000000000000000000001                                                            
            if((decimalArray[i] << 31) >> 31 ==0){
              printf("0");
              printf("\n");
            }
            else if((decimalArray[i] << 31) >> 31 ==1){
              printf("5");
              printf("\n");
            }
          }
     	}
	
      }
    }
  }
  return 0;
}
