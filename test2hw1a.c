/* Homework 1, 600.120 Spring 2015

   Reference implementation by Ben Mitchell, ben@cs.jhu.edu
   Programmer: Sang Yeon Kim (skim340)
               Matthew Chen (mchen93)
   I used the solution for hw0
*/

#include <stdio.h>
#include <string.h>
#include <math.h>
#define LENGTH 16

void printBinary(char course[]){
  course[LENGTH-1] = '\0';   // make sure last character is null
  char c = course[1]; // second char is enough to determine division
  unsigned int div; // course division
  //binary converstion
  // NOTE: could have done this with a switch(); see
  // the way we do 'mark' below.
  if (c == 'S') { // AS
    div = 0;
  } else if (c == 'U') { // BU
    div = 1;
  } else if (c == 'D') { // ED
    div = 2;
  } else if (c == 'N') { // EN
    div = 3;
  } else if (c == 'E') { // ME
    div = 4;
  } else if (c == 'H') { // PH
    div = 5;
  } else if (c == 'Y') { // PY
    div = 6;
  } else if (c == 'A') { // SA
    div = 7;
  } else { // default case for invalid input
    div = -1;
  }
  div = (div << 29);
   unsigned int department; // course department code
  // convert each digit to an integer [0-9], then multiply to put it in the
  // propper column
  department = (course[3] - '0') * 100 + (course[4] - '0') * 10 + (course[5] - '0');
  department = (department << 19);
   unsigned int number; // course number, handled same way as department code
  number = (course[7] - '0') * 100 + (course[8] - '0') * 10 + (course[9] - '0');
  number = (number << 9);
   unsigned int grade; // course letter grade
  c = course[10];
  if (c > 'Z') { // check for lower case
    c += 'A' - 'a'; // convert to upper case
  }

  // convert to int [0-7]
  switch(c) {
  case 'F':
    grade = 4;
    break;
  case 'I':
    grade = 5;
    break;
  case 'S':
    grade = 6;
    break;
  case 'U':
    grade = 7;
    break;
  default:
    grade = c - 'A'; // this handles A-D
  }
  
  grade = (grade << 6);

  unsigned int mark; // course letter grade modifier (+,-,/)
  c = course[11];
  switch (c) {
  case '+':
    mark = 0;
    break;
  case '-':
    mark = 1;
    break;
  case '/':
    mark = 2;
    break;
  default:
    mark = -1; // invalid input
  }

  mark = (mark << 4);

   unsigned int credits; // number of credits (whole number part)
  credits = course[12] - '0';
  credits = (credits << 1);
   unsigned int halfCredits = 0; // number of half credits
  if (course[14] == '5') {
    halfCredits = 1;
  } 
  halfCredits = (halfCredits << 0);// no need for else because we initialized it to a default value
   /* mandatory output: */
  
  FILE* output = fopen("courseInts.txt", "a");

  unsigned int bitSum = div + department + number + grade + mark + credits + halfCredits; 
  int position = 31;
  char binary[32];
  printf("%s  ", course);
  printf("%u  ", bitSum);
  fprintf(output,"%u\n",bitSum);
  fclose(output);
  while(position >= 0){
    binary[position] = bitSum % 2 + '0';
    bitSum /= 2;
    position --;
  }
  for (int i = 0; i < 32; i++){
    printf("%c",binary[i]);
  }

  printf("\n");
  
  

  

}


int main(int argc, char* argv[])
{
  FILE* input = fopen(argv[1],"r");
   if (argc == 1) {
    printf("Usage: hw0 XX.###.###Gg#.#\n");
    return 1;  // exit program
  }

  char courseArray[2500][LENGTH + 1];
  int coursePosition;
  coursePosition = 0;
  while(fscanf(input,"%s",(char*)&(courseArray[coursePosition])) != EOF){
    printBinary(courseArray[coursePosition]);
    coursePosition ++;
  }

  //strncpy(course, argv[1], LENGTH);  // copy to course string
  //printf("course string: %s\n", course);  // echo input

  /*
   * Format and layout of expected input is:
   * X X . # # # . # # # G  g  #  .  # 
   * 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14
   *
   * Note that we don't do much in the way of error checking;
   * bad input produces undefined output.
   */
}
